logger_config = {
    'name': 'gs_range.log',
    'log_path': '/tmp',
    'rotate_bytes': 1000000,
    'rotate_count': 3,
    'file_level': 10,
    'console_level': 10
}

app_config = {
    'oneshot_file': '/tmp/adhoc',
    'everyday_times': '/tmp/everyday.json',
    'today_times': '/tmp/today.json',
    'oneshot_intervals': '/tmp/adhoc.json'
}