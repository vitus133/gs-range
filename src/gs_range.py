
import requests
import json
import os
import pandas as pd
import datetime
import argparse
from datetimerange import DateTimeRange
from logger import Logger
from config import app_config

"""
This module checks whether the current time is in range of times
defined by user in a google sheet or by ad-hoc command.
It can be used for turning an appliance on or off.
"""


class UserInput(Logger):
    """This class handles inputs from users

    The inputs are received through Google sheets and one shot commands.
    This class saves the intermediate results in json files, so other
    classes using them as inputs can be separately invoked by cron
    """
    def __init__(self, secrets_path, oneshot_min):
        """
        Get and parse user input
        """
        self.sp = secrets_path
        self.oneshot = app_config.get('oneshot_file')
        self.oneshot_min = oneshot_min
        # Get weekday name and current minute in the day
        self.now = datetime.datetime.now()
        self.weekday = self.now.strftime("%A")

    def process_inputs(self):
        self.set_gs_url()
        self.get_gsh()
        self.export_ranges()
        self.handle_oneshots()

    def __get_secrets(self):
        """ Get api key and workbook id from secrets file
        """
        try:
            if not os.path.exists(self.sp):
                self.logger.error("Secrets file is missing, exiting")
                exit(1)
            with open(self.sp, "r") as sf:
                self.secrets = json.load(sf)
        except Exception as e:
            self.logger.exception(e)

    def set_gs_url(self):
        self.__get_secrets()
        try:
            workbook_id = self.secrets.get("workbook_id")
            key = self.secrets.get("key")
            sheet_name = "Times"
            base_url = "https://sheets.googleapis.com/v4/spreadsheets"
            self.url = (
                f"{base_url}/{workbook_id}/values/"
                f"{sheet_name}?key={key}")
        except Exception as e:
            self.logger.exception(e)

    def get_gsh(self):
        """Get the on/off times from google sheets

        Ther result is saved in a json file available for
        subsequent calls in case of network instability.
        This can also be used to reduce the amount of
        API calls if needed.
        """
        try:
            r = requests.get(self.url)
            if r.status_code != 200:
                self.logger.exception("Error getting the workbook",
                                      r.text, r.status_code, r.reason)
                self.raw_values = []
                return
            self.raw_values = r.json().get("values")
        except Exception as e:
            self.logger.exception(e)

    def export_ranges(self):
        """
        Transform gsheet data to time ranges

        Transform json time ranges received from google sheets
        to separate time ranges in text for every day and today.
        Save as json files
        """

        df = pd.DataFrame.from_dict(self.raw_values)
        everyday = df.drop(range(2, 16), axis=1).drop(
            [0, 1], axis=0).values.tolist()
        today_idx = list(df.loc[0, :]).index(self.weekday)
        today = df[2:].transpose()[today_idx:today_idx+2]\
            .transpose().dropna().values.tolist()
        with open(app_config.get('everyday_times'), "w") as edf:
            json.dump(everyday, edf)
        with open(app_config.get('today_times'), "w") as tdf:
            json.dump(today, tdf)

    def handle_oneshots(self):
        """Ad-hoc "appliance on" can be requested by usser through Siri"""
        # Check if adhoc request file exists
        # if yes and now is not in range, delete it
        if os.path.exists(app_config.get('oneshot_intervals')):
            with open(app_config.get('oneshot_intervals'), "r") as ahf:
                ah = json.load(ahf)
            dr = DateTimeRange(ah[0][0], ah[0][1])
            if self.now not in dr:
                os.remove(app_config.get('oneshot_intervals'))

        # Check if "appliance on" requested by user
        adhoc = []
        # "adhoc" file will be created by Siri shortcut via the SSH
        if os.path.exists(self.oneshot):
            delta = datetime.timedelta(minutes=self.oneshot_min)

            adhoc.append(
                [self.now.strftime("%H:%M"),
                    (self.now + delta).strftime("%H:%M")])
            os.remove(self.oneshot)
            with open(app_config.get('oneshot_intervals'), "w") as ah:
                json.dump(adhoc, ah)


class OnOffLogic(Logger):
    """
    Combines on and off times into time ranges when the appliance
    must be off. Checks whether the appliance must be on or off now
    """
    def __init__(self):
        with open(app_config.get('everyday_times'), "r") as edf:
            everyday = json.load(edf)
        with open(app_config.get('today_times'), "r") as tdf:
            today = json.load(tdf)
        if os.path.exists(app_config.get('oneshot_intervals')):
            with open(app_config.get('oneshot_intervals'), "r") as adf:
                adhoc = json.load(adf)
        else:
            adhoc = []

        self.logger.debug(
            f"Adhoc times: {adhoc}, "
            f"Today times: {today}, "
            f"Everyday times: {everyday}")

        ranges = []
        for item in everyday + today + adhoc:
            try:
                if all(item):  # This will skip empty and NaT values
                    ranges.append(DateTimeRange(item[0], item[1]))
            except ValueError:
                self.logger.warning(f"Malformed {item}")
        self.inrange = any(
            [datetime.datetime.now() in r for r in ranges])
        self.logger.debug(
            f"Ensure appliance is {'on' if self.inrange else 'off'}")


if __name__ == '__main__':
    """
    Running from the main folder:
    python src/gs_range.py -m 1 -s $(pwd)/secrets.json
    """
    my_parser = argparse.ArgumentParser(
        prog='gs_range',
        usage='%(prog)s [options]',
        description='''Finds whether the current time is in range of
        the user-defined times. ''')
    my_parser.add_argument(
        '--secrets', '-s', type=str, default='/etc/gs_range/secrets.json',
        help="Full path of the secrets file")
    my_parser.add_argument(
        '--oneshot-minutes', '-m', type=int, default=30,
        help="Number of minutes for the ad-hoc one-shot appliance activation")

    args = my_parser.parse_args()
    ui = UserInput(args.secrets, args.oneshot_minutes)
    ui.process_inputs()
    bl = OnOffLogic()
    print(bl.inrange)
