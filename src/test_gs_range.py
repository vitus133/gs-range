import unittest
import sys
import os
import json
import tempfile
from pathlib import Path
from config import app_config


from gs_range import UserInput


class TestUserInput(unittest.TestCase):
    def setUp(self):
        """
        Init sample data and secrets path
        """
        self.gs_content_fixture = [
            ["Everyday", "", "Sunday", "", "Monday", "",
             "Tuesday", "", "Wednesday", "", "Thursday", "",
             "Friday", "", "Saturday"],
            ["On", "Off", "On", "Off", "On",
             "Off", "On", "Off", "On", "Off", "On",
             "Off", "On", "Off", "On", "Off"],
            ["7:00", "7:30", "", "", "", "", "11:00",
             "12:00", "", "", "12:10", "12:20", "12:10",
             "12:20", "11:00", "12:00"],
            ["20:00", "21:00", "", "", "", "", "22:00", "23:00"]]
        self.secrets_path = tempfile.NamedTemporaryFile(
            delete=False).name
        with open(self.secrets_path, 'w') as sp:
            json.dump({"workbook_id": "dummy", "key": "dummy"}, sp)

        self.ui = UserInput(self.secrets_path, 10)

    def test_init(self):
        self.assertEqual(self.ui.sp, self.secrets_path)
        self.assertEqual(self.ui.oneshot, app_config.get('oneshot_file'))
        self.assertEqual(self.ui.oneshot_min, 10)

    def test_set_gs_url(self):
        self.ui.set_gs_url()
        self.assertEqual(
            self.ui.url,
            "https://sheets.googleapis.com/v4/spreadsheets/"
            "dummy/values/Times?key=dummy")

    def test_export_ranges(self):
        self.ui.raw_values = self.gs_content_fixture
        self.ui.weekday = "Saturday"
        self.ui.export_ranges()
        with open(app_config.get('everyday_times'), 'r') as ed:
            edc = json.load(ed)
        self.assertEqual(edc, [["7:00", "7:30"], ["20:00", "21:00"]])
        with open(app_config.get('today_times'), 'r') as ed:
            edc = json.load(ed)
        self.assertEqual(edc, [["11:00", "12:00"]])

    def test_adhoc(self):
        Path(app_config.get('oneshot_file')).touch()
        self.ui.handle_oneshots()
        self.assertFalse(os.path.exists(app_config.get('oneshot_file')))
        os.unlink(app_config.get('oneshot_intervals'))

    def tearDown(self):
        os.unlink(self.secrets_path)


if __name__ == '__main__':
    unittest.main()
