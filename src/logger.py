import logging
from pathlib import PurePath
from logging.handlers import RotatingFileHandler
from config import logger_config

class Logger():
    @property
    def logger(self):
        name = '.'.join([
            logger_config.get('name').split('.')[0]
        ])
        lg = logging.getLogger(name)
        lg.setLevel(10)
        formatter = logging.Formatter(
            '%(name)s %(asctime)s %(levelname)s [%(module)s:%(lineno)s]: %(message)s',
            datefmt='%Y-%m-%d %H:%M:%S')
        if not lg.hasHandlers():
            # logging to file
            path = PurePath(
                logger_config.get('log_path'), logger_config.get('name'))    
            handler = RotatingFileHandler(
                str(path),
                maxBytes=logger_config.get('rotate_bytes'),
                backupCount=logger_config.get('rotate_count'))
            handler.setLevel(logger_config['file_level'])
            handler.setFormatter(formatter)
            lg.addHandler(handler)

            # logging to console
            handler = logging.StreamHandler()
            handler.setLevel(logger_config['console_level'])
            handler.setFormatter(formatter)
            lg.addHandler(handler)
        return lg