env: venv/bin/activate
venv/bin/activate: requirements.txt
	python3 -m venv venv
	./venv/bin/pip install -r requirements.txt

clean:
	rm -rf build dist __pycache__ src/__pycache__ *.spec

test: venv/bin/activate
	venv/bin/python -m unittest  discover -v  ./src

run: venv/bin/activate
	venv/bin/python src/gs_range.py -s secrets.json

build: venv/bin/activate
	venv/bin/pyinstaller --onefile --strip src/gs_range.py
