# gs-range

GS Range is an auxiliary program that helps to determine whether current day of week and time is in a range defined in a Google sheet. 
It aims to be a part of a bigger automation system, such as electric water boilers, smart irrigation etc.

# Overview
## User input
The user input can be provided in two ways: via a Google sheet for scheduled operations, and via a one-shot command. The latter is implemented in my sustem with Siri and Shortcuts application, but can be done in myriad of ways, as will be explaned below.
### Google sheets - time ranges
The time ranges can be defined in a spreadsheet as shown below:
<img src="bitmap/boiler_times.png" width=800>

The workbook must contain a sheet named `Times` (case sensitive). The header must look as shown on the picture, i.e. it must contain a column `Everyday`, and then a column for each weekday.
The `Everyday` column and any of the weekday columns can include any sensible number of `On` and `Off` times, defining the appliance operation intervals.
The spreadsheet must be shared to anyone in a view-only mode (and only to specific people in the Edit mode)

### One-shot requests
One-shot requests are realized via the iOS `Shortcuts` app, as shown below.

<img src="bitmap/shortcut.png" width=300>

When I say "Hey Siri, turn on the boiler", the shortcut runs and creates an empty file in my boiler controller.
This file is then being discovered by the application, and a one-shot time interval for the boiler operation is created.


# Operation
The program runs using a cron job every minute and determines whether the appliance should be On or Off It returns `True` or `False` on STDOUT (whether the appliance should be On or Off respectively)

# Configuration
1. Create your 'secrets.json' file. The template is provided in `secrets.json.sample`.
  - Get your API key from Google
  - Get your configuration spreadsheeet and copy its ID
2. Edit `src/config.py` to suit your needs
# Hack
1. Clone the repo.
2. To create a virtual environment, use
```shell
make env
```
3. To run the software, use
```shell
make run
```
4. To run the tests, use
```shell
make test
```
5. To build a standallone executable, use
```shell
make build
```
6. To clean the build / run artifacts, use
```shell
make clean
```

# Future development plans
### Remote logging
My boiler controller running on Raspberry Pi Zero W is using read-only file system. Need to ship logs somewhere to a remote server





