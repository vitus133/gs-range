#!/bin/bash

# Common path for all GPIO files
BASE_GPIO_PATH=/sys/class/gpio

BOILER=17
ON="1"
OFF="0"
TRISTATE="3"
OUTPUT="out"
INPUT="in"

# Export the pin if not already exported
export_pin()
{
  if [ ! -e $BASE_GPIO_PATH/gpio$1 ]; then
    echo "$1" > $BASE_GPIO_PATH/export
  fi
}

# Set the pin mode (input or output)
# Parameters: $1 - pin number
#             $2 - desired mode
set_mode()
{
  if [[ ! $(cat $BASE_GPIO_PATH/gpio$1/direction) == $2 ]]; then
    echo $2 > $BASE_GPIO_PATH/gpio$1/direction
  fi
}





# Change the pin state
set_pin_state()
{
  case $2 in

    $ON | $OFF)
      set_mode $BOILER $OUTPUT
      echo $2 > $BASE_GPIO_PATH/gpio$1/value
      ;;

    $TRISTATE)
      set_mode $BOILER $INPUT
      ;;

    *)
      echo -n "Unknown pin state"
      ;;
  esac
}



# Export the pin
export_pin $BOILER

RESULT=$(/home/pi/gs_range/venv/bin/python /home/pi/gs_range/src/gs_range.py | tail -1)
if [[ $RESULT == "True" ]]; then
  set_pin_state $BOILER $ON
else
  set_pin_state $BOILER $TRISTATE
fi

